export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const SET_MESSAGE = 'SET_MESSAGE';
export const SET_INIT = 'SET_INIT';

export const LOGOUT = 'LOGOUT';

export const CLEAR_MESSAGE = 'CLEAR_MESSAGE';


import React, { Component, useState, useEffect } from 'react'
import { Text, Title, Divider, Card, Button } from 'react-native-elements'
import { View, RefreshControl, TouchableOpacity } from 'react-native'
import { useIsFocused } from '@react-navigation/native'
import axios from 'axios'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ScrollView } from 'react-native-gesture-handler'
import { SocialIcon } from 'react-native-elements'
import AsyncStorage from '@react-native-community/async-storage';


export default function Main({ route, navigation }) {
    const isFocused = useIsFocused();
    const [userId, setUserId] = useState();
    const [isLoading, setIsLoading] = useState(true);
    const [token, setToken] = useState('');
    const [data, setData] = useState([{
        id: 0, title: "", content: "", user: [{ id: 0, name: "" }]
    }]);
    const getAsync = async () => {
        try {
            setUserId(await AsyncStorage.getItem('userId'));
        } catch (error) {
            console.log(error);
        }
    }
    const postToken = async (e) => {
        axios
            .get('https://user-article.herokuapp.com/api/articles')
            .then(async function (res) {
                console.log(res.data.article);
                setData(res.data.article);
                setToken(await AsyncStorage.getItem('token'))
                setIsLoading(false)
            })
            .catch(function (error) {
                setIsLoading(false)
                alert(error);
                console.log(JSON.stringify(error));

            })
    }

    const DeleteArtikel = (id) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
        axios
            .delete(`https://user-article.herokuapp.com/api/articles/${id}`,
                {
                    headers: header
                }
            )
            .then(function (res) {
                alert('Data Sambited')
                setIsLoading(true)
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }

    const checkToken = async () => {
        if (await AsyncStorage.getItem('token') == null) {
            alert('blom login mau bikin artikel, login dulu cok')
        } else {
            navigation.navigate('createArtikel')
        }
    }

    useEffect(() => {
        postToken()
        getAsync()
    }, [isFocused, isLoading]);

    function isUserArticle(id, artikelId) {
        if (userId == id) {
            return (<>
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                        style={{ marginBottom: 10 }}
                        onPress={() => navigation.navigate('editArticle', { idartikel: artikelId })}>
                        <Icon
                            name='pen'
                            size={20}
                            color='#2a9df4'

                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ marginBottom: 10, marginLeft: 15 }}
                        onPress={() => DeleteArtikel(artikelId)}>
                        <Icon
                            name='trash'
                            size={20}
                            color='red'

                        />
                    </TouchableOpacity>
                    <Divider />
                </View>
            </>
            )
        }
    }
    return (
        <>
            <ScrollView
                refreshControl={
                    <RefreshControl refreshing={isLoading} onRefresh={() => setIsLoading(true)} />
                }
            >
                {data.length === 0 ? (

                    <Text>THERE ARE NO DATA</Text>) : (
                        data.map((artikel) => {
                            return (

                                <><Card>
                                    {isUserArticle(artikel.user.id, artikel.id)}
                                    <Card.Title h4 key={artikel.id}>{artikel.title}
                                    </Card.Title>

                                    <Card.Title h5>Writer: {artikel.user.name} </Card.Title>
                                    <Card.Divider />
                                    <Card.FeaturedSubtitle>
                                        <Text>{artikel.content.substring(0, 150)}...
                                    <Text style={{ color: 'blue' }} onPress={() => navigation.navigate('detailArticle', { idnya: artikel.id })}>Read more</Text>
                                        </Text>
                                    </Card.FeaturedSubtitle>
                                </Card>
                                </>
                            )

                        })
                    )
                }

            </ScrollView>
            <SocialIcon
                raised={true}
                iconColor="white"
                onPress={checkToken}
                type='twitter'
                style={{ position: 'absolute', right: 15, bottom: 15 }}
            />
        </>
    );
}


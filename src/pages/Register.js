

import { NavigationContainer } from '@react-navigation/native';
import React, { useState } from 'react';
import { View } from 'react-native';
import { Input, Card, Button, Text } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';



const Register = ({ navigation }) => {

    const [nama, setNama] = useState('');
    const [uname, setUname] = useState('');
    const [mel, setMel] = useState('');
    const [pass, setPass] = useState('');
    const [conf, setConf] = useState('');
    const [isLoading, setIsLoading] = useState(false)

    const postRegis = async (e) => {
        setIsLoading(true)
        axios
            .post('https://user-article.herokuapp.com/api/auth/signup', {
                name: nama,
                username: uname,
                email: mel,
                password: pass,
            })
            .then(function (res) {

                console.log('res ' + res.data);
                navigation.navigate('login');
                alert('Data Sambited')
                setIsLoading(false)
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(SON.stringify(error.response, null, 2));
                setIsLoading(false)
            });

    }

    return (
        <>
            <View style={{ justifyContent: 'center' }}>

                <Card >
                    <Card.Title h4>Register</Card.Title>
                    <Card.Divider />
                    <Input
                        placeholder='name'
                        leftIcon={
                            <Icon
                                name='user-o'
                                size={18}
                                color='grey'
                            />
                        }
                        onChangeText={nama => setNama(nama)}
                    />
                    <Input
                        placeholder='username'
                        leftIcon={
                            <Icon
                                name='user'
                                size={20}
                                color='grey'
                            />
                        }
                        onChangeText={uname => setUname(uname)}
                    />
                    <Input
                        placeholder='email'
                        leftIcon={
                            <Icon
                                name='envelope'
                                size={20}
                                color='grey'
                            />
                        }
                        onChangeText={mel => setMel(mel)}
                    />

                    <Input
                        placeholder='password'
                        secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='lock'
                                size={20}
                                color='grey'
                            />
                        }
                        onChangeText={pass => setPass(pass)}
                    />
                    {/* <Input
                        placeholder='confirm password'
                        secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='unlock-alt'
                                size={20}
                                color='grey'
                            />
                        }
                        onChangeText={conf => setConf(conf)}
                    /> */}

                    <Button
                        icon={{
                            name: "check",
                            size: 20,
                            color: "white"
                        }}
                        loading={isLoading}
                        title="Sambit"
                        onPress={() => postRegis()}
                    />


                </Card>
            </View>
        </>
    );
};


export default Register;

import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useIsFocused } from '@react-navigation/native'
import { Input, Card, Button, Text } from 'react-native-elements'

import { useDispatch, useSelector } from 'react-redux';
import { logout } from './../store/actions/auth';


const Login = ({ navigation }) => {
    const isFocused = useIsFocused();
    const dispatch = useDispatch();
    const { isLoggedIn, user } = useSelector((state) => state.auth);
    useEffect(() => {
        console.log("ini user anjeng " + user)
        console.log("Sudah Login? ", isLoggedIn);
        if (isLoggedIn === false) {
            navigation.navigate('Login');
        }
    }, [isLoggedIn, isFocused]);
    const onLogout = () => {
        dispatch(logout())
    };

    return (
        <>
            <View style={{ justifyContent: 'center' }}>
                {user === null ? (

                    <Text>THERE ARE NO DATA</Text>) : (
                        <Card >
                            <Card.Title h4>MY ACCOUNT</Card.Title>
                            <Text>Name: {user.name}</Text>
                            <Text>Email: {user.email}</Text>
                            <Text>Email: {user.roles[0]}</Text>
                            <Button title="LOGOUT" onPress={onLogout} />



                        </Card>
                    )
                }
            </View>
        </>
    );
};


export default Login;



import React, { Component, useState, useEffect } from 'react'
import { Text, Title, Divider, Card, Input } from 'react-native-elements'
import { View, TouchableOpacity } from 'react-native'
import { useIsFocused } from '@react-navigation/native'
import axios from 'axios'
import { ScrollView } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';




export default function DetailArticle({ route, navigation }) {
    const { idnya } = route.params;

    const isFocused = useIsFocused();
    const [loading, setLoading] = useState(false);

    const [data, setData] = useState({ user: { id: 0, name: "" }, content: "", createdAt: "" });
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([{}])
    const [token, setToken] = useState(null);

    const getToken = async () => {
        try {
            setToken(await AsyncStorage.getItem('token'))
            console.log("tokenUpdate =" + token);
        } catch (e) {
            console.log('error' + e);
        }
    }

    const postToken = () => {
        axios
            .get(`https://user-article.herokuapp.com/api/articles/${idnya}`)
            .then(function (res) {
                console.log(res.data);
                setData(res.data.article);
                getToken()
            })
            .catch(function (error) {
                console.log(JSON.stringify(error));
            })
    }

    const getComments = () => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
        axios
            .get(`https://user-article.herokuapp.com/api/articles/${idnya}/comments`,

                {
                    headers: header,
                }
            )
            .then(function (res) {
                console.log("INI DATA KOMEN " + JSON.stringify(res.data.article.comments));

                setComments(res.data.article.comments)
            })
            .catch(function (error) {
                console.log(JSON.stringify(error));
            })
    }


    const postComment = () => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
        axios
            .post(`https://user-article.herokuapp.com/api/users/comments/${idnya}`,
                {
                    content: comment,
                },
                {
                    headers: header
                }
            )
            .then(function (res) {
                console.log(JSON.stringify(res));
                alert('JAGA MULUTMU!!')
                loading(!loading)

            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }



    useEffect(() => {
        postToken()
        getToken()
        if (token != null)
            getComments()
    }, [isFocused, loading, token]);


    return (
        <>
            <ScrollView >
                <Text h2 key={data.id}>{data.title}</ Text>
                <Text h5 >Author :{data.user.name}</ Text>
                <Text h5>created at: {data.createdAt.substring(0, 10)}</ Text>
                <Text h5>updated at: {data.updateAt}</ Text>
                < Divider />
                <Text>{data.content}</Text>
                <Divider style={{ marginBottom: 10 }} />
                <View>
                    <Input
                        placeholder='add a comment'
                        rightIcon={
                            <Icon
                                onPress={() => postComment()}
                                name='send'
                                size={20}
                                color='#2a9df4'
                            />

                        }
                        onChangeText={comment => setComment(comment)}
                    />
                </View>
                {comments.length === 0 ? (

                    <Text>THERE ARE NO DATA</Text>) : (
                        comments.map((comments) => {
                            return (
                                <Card>
                                    <Text key={comments.id}>{comments.content}</Text>
                                </Card>
                            )

                        })
                    )
                }
                <Divider />


            </ScrollView>
        </>
    );
}


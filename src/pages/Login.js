import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { useIsFocused } from '@react-navigation/native'
import { Input, Card, Button, Text } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import { useDispatch, useSelector } from 'react-redux';
import { login } from './../store/actions/auth';


const Login = ({ navigation }) => {
    const isFocused = useIsFocused();
    const [uname, setUname] = useState('');
    const [pass, setPass] = useState('');

    const { isLoggedIn } = useSelector((state) => state.auth);
    const dispatch = useDispatch();

    const onHandleSubmit = (e) => {
        dispatch(login(uname, pass))
            .then(() => {
                navigation.navigate('account');
            })
            .catch(() => {
                alert('redugnya eror boi')
            });
    };

    useEffect(() => {
        console.log("Sudah Login? ", isLoggedIn);
        if (isLoggedIn === true) {
            navigation.navigate('Artikel');
        }
    }, [isLoggedIn, isFocused]);

    return (
        <>
            <View style={{ justifyContent: 'center' }}>

                <Card >
                    <Card.Title h4>Login</Card.Title>
                    <Card.Divider />
                    <Input
                        placeholder='username'
                        label='Username'
                        leftIcon={
                            <Icon
                                name='user'
                                size={20}
                                color='grey'
                            />

                        }
                        onChangeText={uname => setUname(uname)}
                    />
                    <Input
                        placeholder='password'
                        label='Password'
                        secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='lock'
                                size={20}
                                color='grey'
                            />

                        }
                        onChangeText={pass => setPass(pass)}
                    />
                    <Button
                        icon={{
                            name: "check",
                            size: 20,
                            color: "white"
                        }}
                        title="Sambit"
                        onPress={(e) => onHandleSubmit(e)}
                    />
                    <Card.Divider />
                    <Text>Do not have account? <Text
                        style={{ color: 'blue' }}
                        onPress={() => navigation.navigate("register")}
                    >
                        Register
                        </Text>
                    </Text>


                </Card>
            </View>
        </>
    );
};


export default Login;

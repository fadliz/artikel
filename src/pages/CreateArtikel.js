
import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { Input, Card, Button, Text } from 'react-native-elements';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';


const CreateArtikel = ({ navigation }) => {

    const [content, setContent] = useState('');
    const [title, setTitle] = useState('');
    const [token, setToken] = useState('');

    const getToken = async () => {
        try {
            setToken(await AsyncStorage.getItem('token'))
            console.log("tokenUpdate =" + token);
        } catch (e) {
            console.log('error' + e);
        }
    }
    useEffect(() => {
        getToken()
    }, []);

    const postArtikel = async (e) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
        axios
            .post('https://user-article.herokuapp.com/api/articles',
                {
                    title: title,
                    content: content,
                },
                {
                    headers: header
                }
            )
            .then(function (res) {

                console.log('res ' + res.data);
                navigation.goBack();
                alert('Data Sambited')
            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }

    return (
        <>
            <View style={{ justifyContent: 'center' }}>

                <Card >
                    <Card.Title h4>Create Article</Card.Title>
                    <Card.Divider />
                    <Input
                        placeholder='Title'
                        onChangeText={title => setTitle(title)}
                    />
                    <ScrollView
                        keyboardShouldPersistTaps="always">
                        <Input
                            placeholder='Content'
                            onChangeText={content => setContent(content)}
                            multiline={true}
                            numberOfLines={17}

                        />
                    </ScrollView>
                    <Button
                        icon={{
                            name: "check",
                            size: 20,
                            color: "white"
                        }}
                        title="Sambit"
                        onPress={() => postArtikel()}
                    />


                </Card>
            </View>
        </>
    );
};


export default CreateArtikel;


import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import { Input, Card, Button, Text } from 'react-native-elements';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';


const EditArticle = ({ route, navigation }) => {
    const { idartikel } = route.params;

    const [content, setContent] = useState('');
    const [title, setTitle] = useState('');
    const [token, setToken] = useState('');
    const [userId, setUserId] = useState();

    useEffect(() => {
        getAsync()
    }, [])
    const getAsync = async () => {
        try {
            setToken(await AsyncStorage.getItem('token'));
            console.log('TOKEN EDIT ', token, idartikel);

        } catch (error) {
            console.log(error);
        }
    }

    const postArtikel = async (e) => {
        const header = {
            'Content-Type': 'application/json',
            'Authorization': token,
        }
        axios
            .put(`https://user-article.herokuapp.com/api/articles/${idartikel}`,
                {
                    title: title,
                    content: content,
                },
                {
                    headers: header
                }
            )
            .then(function (res) {

                console.log('res ' + res.data);
                navigation.goBack();
                alert('Data Editedededed');

            })
            .catch(function (error) {
                console.log(JSON.stringify(error.response, null, 2));
                alert(JSON.stringify(error.response, null, 2));
            });

    }

    return (
        <>
            <View style={{ justifyContent: 'center' }}>

                <Card >
                    <Card.Title h4>Edit Article</Card.Title>
                    <Card.Divider />
                    <Input
                        placeholder='Title'
                        onChangeText={title => setTitle(title)}
                    />
                    <ScrollView
                        keyboardShouldPersistTaps="always">
                        <Input
                            placeholder='Content'
                            onChangeText={content => setContent(content)}
                            multiline={true}
                            numberOfLines={17}

                        />
                    </ScrollView>

                    {/* <Input
                        placeholder='confirm password'
                        secureTextEntry={true}
                        leftIcon={
                            <Icon
                                name='unlock-alt'
                                size={20}
                                color='grey'
                            />
                        }
                        onChangeText={conf => setConf(conf)}
                    /> */}

                    <Button
                        icon={{
                            name: "check",
                            size: 20,
                            color: "white"
                        }}
                        title="Sambit"
                        onPress={() => postArtikel()}
                    />


                </Card>
            </View>
        </>
    );
};


export default EditArticle;

import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import React, { Component } from 'react';
import 'react-native-gesture-handler';
import FirstScreen from './pages/FirstScreen'
import DetailArticle from './pages/DetailArticle'
import Login from './pages/Login'
import Register from './pages/Register'
import CreateArtikel from './pages/CreateArtikel'
import EditArticle from './pages/EditArticle';
import Account from './pages/Account';
// const Tab = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

function Artikel() {
    return (
        <>
            <Stack.Navigator

                screenOptions={{ headerShown: false }}>
                <Stack.Screen
                    name="Home"
                    component={FirstScreen}
                    options={{ title: 'Welcome' }}
                />
                <Stack.Screen
                    name="detailArticle"
                    component={DetailArticle}
                    options={{ title: 'Details' }}
                />
                <Stack.Screen
                    name="createArtikel"
                    component={CreateArtikel}
                    options={{ title: 'Details' }}
                />
                <Stack.Screen
                    name="editArticle"
                    component={EditArticle}
                    options={{ title: 'Edit Article' }}
                />

            </Stack.Navigator>
        </>
    )
}

function akun() {
    return (
        <Stack.Navigator
            initialRouteName="account"
            screenOptions={{ headerShown: false }}>
            <Stack.Screen
                name="account"
                component={Account}
                options={{ title: 'Details' }}
            />
            <Stack.Screen
                name="Login"
                component={Login}
                options={{ title: 'Welcome' }}
            />
            <Stack.Screen
                name="register"
                component={Register}
                options={{ title: 'Details' }}
            />
        </Stack.Navigator>
    )
}

function Navigasi() {
    return (
        <><NavigationContainer>
            <Drawer.Navigator

                screenOptions={{ headerShown: false }}>
                <Drawer.Screen
                    name="Artikel"
                    component={Artikel}
                    options={{ title: 'Article' }}
                />
                <Drawer.Screen
                    name="Akun"
                    component={akun}
                    options={{ title: 'Account' }}
                />

            </Drawer.Navigator>
        </NavigationContainer>
        </>
    )
}

export default Navigasi;
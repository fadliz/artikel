/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Navigasi from './src/Navigasi'


const App = () => {
  return (
    <>
      <Navigasi />
    </>
  );
};


export default App;
